package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {

    public static void main(String[] args) {
        /*
         * The code here does two things: It first creates a new RockPaperScissors
         * -object with the code `new RockPaperScissors()`. Then it calls the `run()`
         * method on the newly created object.
         */
        new RockPaperScissors().run();
    }

    Scanner sc = new Scanner(System.in);
    int roundCounter = 1; // round number
    int humanScore = 0; // score player
    int computerScore = 0; // score robot
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");

    public void run() {
        // TODO: Implement Rock Paper Scissors
        
        // Checks if input is valid
        boolean continueGame = true;
        while (continueGame) {
            String guessPlayer = "";
            System.out.println("Let's play round " + roundCounter);
            while (true) {
                guessPlayer = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
                if (trueValue(guessPlayer)) {
                    break;
                }
                System.out.println("I do not understand " + guessPlayer + ". Could you try again?");
            }

            // Robot makes choise
            Random random = new Random();
            String guessRobot = rpsChoices.get(random.nextInt(rpsChoices.size()));
            
            // Figuers out who won
            if (guessPlayer.equals(guessRobot)) {
                System.out.println("Human chose " + guessPlayer + ", computer chose " + guessRobot + ". It's a tie!");
            } else if (whoWon(guessPlayer, guessRobot)) {
                System.out
                        .println("Human chose " + guessPlayer + ", computer chose " + guessRobot + ". Computer wins!");
                computerScore++;
            } else {
                System.out.println("Human chose " + guessPlayer + ", computer chose " + guessRobot + ". Human wins!");
                humanScore++;
            }

            roundCounter++;
            System.out.println("Score: human " + humanScore + ", computer " + computerScore);

            // Checks if the player wants to play another round
            String newRound = readInput("Do you wish to continue playing? (y/n)?").toLowerCase();
            if (!newRound.equals("y")) {
                System.out.println("Bye bye :)");
                break;
            }
        }
    }

    /**
     * 
     * @param answer
     * @return true if the input is valid
     */
    Boolean trueValue(String answer) {
        if (answer.equals("rock") || answer.equals("paper") || answer.equals("scissors")) {
            return true;
        }
        return false;
    }

    /**
     * 
     * @param guessPlayer
     * @param guessRobot
     * @return returns true if robot won
     */
    Boolean whoWon(String guessPlayer, String guessRobot) {
        if (guessRobot.equals("rock") && guessPlayer.equals("scissors")) {
            return true;
        } else if (guessRobot.equals("paper") && guessPlayer.equals("rock")) {
            return true;
        } else if (guessRobot.equals("scissors") && guessPlayer.equals("paper")) {
            return true;}
        return false;
    }

    /**
     * Reads input from console with given prompt
     * 
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
